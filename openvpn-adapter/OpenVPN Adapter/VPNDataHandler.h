//
//  VPNDataHandler.h
//  Chat
//
//  Created by Sergey Abramchuk on 15.12.16.
//  Copyright © 2016 Gutch. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const VPNDataHandlerProtocolsKey;
extern NSString *const VPNDataHandlerPacketsKey;


@interface VPNDataHandler : NSObject

- (NSDictionary *)handleVPNData:(NSData *)data;
- (NSData *)handleTUNPacket:(NSData *)packet protocol:(NSNumber *)protocol;

@end

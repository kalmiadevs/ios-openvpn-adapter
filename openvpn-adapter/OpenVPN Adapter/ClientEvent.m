//
//  ClientEvent.m
//  Chat
//
//  Created by Sergey Abramchuk on 08.12.16.
//  Copyright © 2016 Gutch. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ClientEvent.h"


@implementation ClientEvent

+ (ClientEvent *)createEvent:(NSString *)name info:(NSString *)info {
    ClientEvent *event = [[ClientEvent alloc] initWithName:name info:info];
    return event;
}

- (instancetype)initWithName:(NSString *)name info:(NSString *)info {
    self = [super init];
    if (self) {
        _identifier = [self getIdentifierFromName:name];
        _info = [info copy];
    }
    return self;
}

- (ClientEventIdentifier)getIdentifierFromName:(NSString *)name {
    NSDictionary *events = @{
        @"DISCONNECTED": @(ClientEventIdentifierDisconnected),
        @"CONNECTED": @(ClientEventIdentifierConnected),
        @"RECONNECTING": @(ClientEventIdentifierReconnecting),
        @"RESOLVE": @(ClientEventIdentifierResolve),
        @"WAIT": @(ClientEventIdentifierWait),
        @"WAIT_PROXY": @(ClientEventIdentifierWaitProxy),
        @"CONNECTING": @(ClientEventIdentifierConnecting),
        @"GET_CONFIG": @(ClientEventIdentifierGetConfig),
        @"ASSIGN_IP": @(ClientEventIdentifierAssignIP),
        @"ADD_ROUTES": @(ClientEventIdentifierAddRoutes),
        @"ECHO": @(ClientEventIdentifierEcho),
        @"INFO": @(ClientEventIdentifierInfo),
        @"PAUSE": @(ClientEventIdentifierPause),
        @"RESUME": @(ClientEventIdentifierResume),
        @"TRANSPORT_ERROR": @(ClientEventIdentifierTransportError),
        @"TUN_ERROR": @(ClientEventIdentifierTunError),
        @"CLIENT_RESTART": @(ClientEventIdentifierClientRestart),
        @"AUTH_FAILED": @(ClientEventIdentifierAuthFailed),
        @"CERT_VERIFY_FAIL": @(ClientEventIdentifierCertVerifyFail),
        @"TLS_VERSION_MIN": @(ClientEventIdentifierTLSVersionMin),
        @"CLIENT_HALT": @(ClientEventIdentifierClientHalt),
        @"CONNECTION_TIMEOUT": @(ClientEventIdentifierConnectionTimeout),
        @"INACTIVE_TIMEOUT": @(ClientEventIdentifierInactiveTimeout),
        @"DYNAMIC_CHALLENGE": @(ClientEventIdentifierDynamicChallenge),
        @"PROXY_NEED_CREDS": @(ClientEventIdentifierProxyNeedCreds),
        @"PROXY_ERROR": @(ClientEventIdentifierProxyError),
        @"TUN_SETUP_FAILED": @(ClientEventIdentifierTunSetupFailed),
        @"TUN_IFACE_CREATE": @(ClientEventIdentifierTunIfaceCreate),
        @"TUN_IFACE_DISABLED": @(ClientEventIdentifierTunIfaceDisabled),
        @"EPKI_ERROR": @(ClientEventIdentifierEPKIError),
        @"EPKI_INVALID_ALIAS": @(ClientEventIdentifierEPKIInvalidAlias),
    };
    
    ClientEventIdentifier event = events[name] != nil ? (ClientEventIdentifier)[(NSNumber *)events[name] unsignedIntegerValue] : ClientEventIdentifierUnknown;
    return event;
}

@end

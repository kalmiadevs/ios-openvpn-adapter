//
//  OpenVPNClient.cpp
//  Chat
//
//  Created by Sergey Abramchuk on 07.12.16.
//  Copyright © 2016 Gutch. All rights reserved.
//

#include <sstream>

#include "OpenVPNClient.h"

#import "ClientWrapper.h"
#import "ClientWrapper+OpenVPN.h"


OpenVPNClient::OpenVPNClient(void *wrapper) : ClientAPI::OpenVPNClient() {
    this->wrapper = wrapper;
}

void OpenVPNClient::connect_pre_run() {
    if (![(__bridge ClientWrapper *)wrapper configureSockets]) {
        throw std::runtime_error("Socket configuration failed");
    }
}

bool OpenVPNClient::tun_builder_new() {
    return true;
}

bool OpenVPNClient::tun_builder_set_remote_address(const std::string &address, bool ipv6) {
    [(__bridge ClientWrapper *)wrapper setRemoteAddress:[NSString stringWithUTF8String:address.c_str()]];
    return true;
}

bool OpenVPNClient::tun_builder_add_address(const std::string &address, int prefix_length, const std::string &gateway, bool ipv6, bool net30) {
    NSString *localAddress = [NSString stringWithUTF8String:address.c_str()];
    NSString *subnet = [NSString stringWithUTF8String:get_subnet(prefix_length).c_str()];
    NSString *gatewayAddress = [NSString stringWithUTF8String:gateway.c_str()];
    
    [(__bridge ClientWrapper *)wrapper setLocalAddress:localAddress subnet:subnet gateway:gatewayAddress];
    return true;
}

bool OpenVPNClient::tun_builder_reroute_gw(bool ipv4, bool ipv6, unsigned int flags) {
    return true;
}

bool OpenVPNClient::tun_builder_add_route(const std::string& address, int prefix_length, int metric, bool ipv6) {
    NSString *route = [NSString stringWithUTF8String:address.c_str()];
    NSString *subnet = [NSString stringWithUTF8String:get_subnet(prefix_length).c_str()];
    
    [(__bridge ClientWrapper *)wrapper addRoute:route subnet:subnet];
    return true;
}

bool OpenVPNClient::tun_builder_exclude_route(const std::string& address, int prefix_length, int metric, bool ipv6) {
    NSString *route = [NSString stringWithUTF8String:address.c_str()];
    NSString *subnet = [NSString stringWithUTF8String:get_subnet(prefix_length).c_str()];
    
    [(__bridge ClientWrapper *)wrapper excludeRoute:route subnet:subnet];
    return true;
}

bool OpenVPNClient::tun_builder_add_dns_server(const std::string& address, bool ipv6) {
    [(__bridge ClientWrapper *)wrapper addDNSAddress:[NSString stringWithUTF8String:address.c_str()]];
    return true;
}

bool OpenVPNClient::tun_builder_add_search_domain(const std::string& domain) {
    [(__bridge ClientWrapper *)wrapper addSearchDomain:[NSString stringWithUTF8String:domain.c_str()]];
    return true;
}

bool OpenVPNClient::tun_builder_set_mtu(int mtu) {
    [(__bridge ClientWrapper *)wrapper setMTU:mtu];
    return true;
}

bool OpenVPNClient::tun_builder_set_session_name(const std::string& name) {
    return true;
}

bool OpenVPNClient::tun_builder_add_proxy_bypass(const std::string& bypass_host) {
    return true;
}

bool OpenVPNClient::tun_builder_set_proxy_auto_config_url(const std::string& url) {
    return true;
}

bool OpenVPNClient::tun_builder_set_proxy_http(const std::string& host, int port) {
    return true;
}

bool OpenVPNClient::tun_builder_set_proxy_https(const std::string& host, int port) {
    return true;
}

bool OpenVPNClient::tun_builder_add_wins_server(const std::string& address) {
    return true;
}

int OpenVPNClient::tun_builder_establish() {
    return (int)[(__bridge ClientWrapper *)wrapper establishTunnel];
}

bool OpenVPNClient::tun_builder_persist() {
    return true;
}

void OpenVPNClient::tun_builder_establish_lite() {
    
}

void OpenVPNClient::tun_builder_teardown(bool disconnect) {
    
}

bool OpenVPNClient::socket_protect(int socket) {
    return true;
}

void OpenVPNClient::external_pki_cert_request(ClientAPI::ExternalPKICertRequest& certreq) { }
void OpenVPNClient::external_pki_sign_request(ClientAPI::ExternalPKISignRequest& signreq) { }

bool OpenVPNClient::pause_on_connection_timeout() {
    return false;
}

void OpenVPNClient::event(const ClientAPI::Event& ev) {
    [(__bridge ClientWrapper *)wrapper handleEvent:&ev];
}

void OpenVPNClient::log(const ClientAPI::LogInfo& log) {
    [(__bridge ClientWrapper *)wrapper handleLog:&log];
}

std::string OpenVPNClient::get_subnet(int prefix_length) {
    uint32_t bitmask = UINT_MAX << (sizeof(uint32_t) * 8 - prefix_length);
    
    uint8_t first = (bitmask >> 24) & 0xFF;
    uint8_t second = (bitmask >> 16) & 0xFF;
    uint8_t third = (bitmask >> 8) & 0xFF;
    uint8_t fourth = bitmask & 0xFF;
    
    std::stringstream stream;
    stream << std::to_string(first) << "." << std::to_string(second) << "." << std::to_string(third) << "." << std::to_string(fourth);
    
    return stream.str();
}

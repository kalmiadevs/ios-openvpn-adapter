//
//  ClientWrapper+Private.h
//  Chat
//
//  Created by Sergey Abramchuk on 07.12.16.
//  Copyright © 2016 Gutch. All rights reserved.
//

#include "OpenVPNClient.h"


@interface ClientWrapper (OpenVPN)

- (BOOL)configureSockets;
- (void)setRemoteAddress:(NSString *)address;
- (void)setLocalAddress:(NSString *)address subnet:(NSString *)subnet gateway:(NSString *)gateway;
- (void)addRoute:(NSString *)route subnet:(NSString *)subnet;
- (void)excludeRoute:(NSString *)route subnet:(NSString *)subnet;
- (void)addDNSAddress:(NSString *)address;
- (void)addSearchDomain:(NSString *)domain;
- (void)setMTU:(NSInteger)mtu;
- (NSInteger)establishTunnel;

- (void)handleEvent:(const ClientAPI::Event *)event;
- (void)handleLog:(const ClientAPI::LogInfo *)log;

@end

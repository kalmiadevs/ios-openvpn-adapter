//
//  ClientWrapper.m
//  Chat
//
//  Created by Sergey Abramchuk on 07.12.16.
//  Copyright © 2016 Gutch. All rights reserved.
//

#import <sys/socket.h>
#import <sys/un.h>
#import <sys/stat.h>
#import <sys/ioctl.h>
#import <arpa/inet.h>

#import <NetworkExtension/NetworkExtension.h>

#import "ClientWrapper.h"
#import "ClientWrapper+OpenVPN.h"
#import "VPNDataHandler.h"

#include "OpenVPNClient.h"

NSString *const ClientWrapperErrorDomain = @"ClientWrapperErrorDomain";

NSString *const ClientWrapperErrorFatalKey = @"ClientWrapperErrorFatalKey";

NSString *const VPNSettingsRemoteAddressKey = @"VPNSettingsRemoteAddressKey";
NSString *const VPNSettingsLocalAddressKey = @"VPNSettingsLocalAddressKey";
NSString *const VPNSettingsSubnetMaskKey = @"VPNSettingsSubnetMaskKey";
NSString *const VPNSettingsGatewayKey = @"VPNSettingsGatewayKey";
NSString *const VPNSettingsIncludedRoutesKey = @"VPNSettingsIncludedRouteKey";
NSString *const VPNSettingsExcludedRoutesKey = @"VPNSettingsExcludedRouteKey";
NSString *const VPNSettingsDNSAddressesKey = @"VPNSettingsDNSAddressesKey";
NSString *const VPNSettingsSearchDomainsKey = @"VPNSettingsSearchDomainsKey";
NSString *const VPNSettingsMTUKey = @"VPNSettingsMTUKey";


@interface ClientWrapper ()

@property (strong, nonatomic) NSMutableDictionary *vpnSettings;
@property OpenVPNClient *vpnClient;

@property CFSocketRef wrapperSocket;
@property CFSocketRef vpnSocket;

@property (strong, nonatomic) VPNDataHandler *dataHandler;

@property (weak, nonatomic) NEPacketTunnelFlow *packetFlow;

@end


@implementation ClientWrapper

#pragma mark Initialization

- (instancetype)initWithUsername:(NSString *)username password:(NSString *)password configurationFile:(NSData *)configurationFile {
    self = [super init];

    if (self) {
        self.vpnSettings = [NSMutableDictionary new];
        self.dataHandler = [VPNDataHandler new];

        if (![self configureClientWithUsername:username password:password configurationFile:configurationFile]) {
            return nil;
        }
    }

    return self;
}

#pragma mark Sockets Configuration

// TODO: Add workaround description

static void socketCallBack(CFSocketRef socket, CFSocketCallBackType type, CFDataRef address, const void *data, void *info) {
    ClientWrapper *wrapper = (__bridge ClientWrapper *)info;

    switch (type) {
        case kCFSocketDataCallBack:
            [wrapper readVPNData:(__bridge NSData *)data];
            break;

        default:
            break;
    }
}

- (BOOL)configureSockets {
    int sockets[2];
    if (socketpair(PF_LOCAL, SOCK_DGRAM, IPPROTO_IP, sockets) == -1) {
        NSLog(@"Failed to create a pair of connected sockets due to error: %@", [NSString stringWithUTF8String:strerror(errno)]);
        return NO;
    }

    CFSocketContext socketCtxt = {0, (__bridge void *)self, NULL, NULL, NULL};

    self.wrapperSocket = CFSocketCreateWithNative(kCFAllocatorDefault, sockets[0], kCFSocketDataCallBack, &socketCallBack, &socketCtxt);
    self.vpnSocket = CFSocketCreateWithNative(kCFAllocatorDefault, sockets[1], kCFSocketNoCallBack, NULL, NULL);

    if (!self.wrapperSocket || !self.vpnSocket) {
        NSLog(@"Failed to create sockets for communication");
        return NO;
    }

    CFRunLoopSourceRef wrapperSource = CFSocketCreateRunLoopSource(kCFAllocatorDefault, self.wrapperSocket, 0);
    CFRunLoopAddSource(CFRunLoopGetMain(), wrapperSource, kCFRunLoopCommonModes);

    CFRelease(wrapperSource);

    return YES;
}

#pragma mark Client Configuration

- (BOOL)configureClientWithUsername:(NSString *)username password:(NSString *)password configurationFile:(NSData *)configurationFile {
    
    NSString *vpnConfiguration = [[NSString alloc] initWithData:configurationFile encoding:NSUTF8StringEncoding];
    
    if (vpnConfiguration == nil) {
        NSLog(@"KALMIA: errror ");
    }
    NSAssert(vpnConfiguration != nil, @"Failed to get content of the VPN configuration file due to error");

    self.vpnClient = new OpenVPNClient((__bridge void *)self);

    ClientAPI::Config configuration;
    configuration.content = std::string([vpnConfiguration UTF8String]);
    configuration.connTimeout = 30;

    ClientAPI::EvalConfig eval = self.vpnClient->eval_config(configuration);
    
    if (eval.error == true) {
        NSLog(@"KALMIA: Failed to parse VPN configuration file");
    }
    NSAssert(eval.error == false, @"Failed to parse VPN configuration file. Returned message: %@", [NSString stringWithUTF8String:eval.message.c_str()]);

    ClientAPI::ProvideCreds creds;

    creds.username = [username UTF8String];
    creds.password = [password UTF8String];

    ClientAPI::Status creds_status = self.vpnClient->provide_creds(creds);
    if (creds_status.error) {
        NSLog(@"Failed to provide credentials");
        return NO;
    }

    return YES;
}

#pragma mark Connection Control

- (void)connect {
    NSAssert(self.delegate != nil, @"ClientWrapper delegate should be set");

    dispatch_queue_t connectQueue = dispatch_queue_create("com.example.vpnprovider.connect", NULL);
    dispatch_async(connectQueue, ^{
        try {
            OpenVPNClient::init_process();

            ClientAPI::Status status = self.vpnClient->connect();
            if (status.error) {
                NSError *error = [NSError errorWithDomain:ClientWrapperErrorDomain
                                                     code:ClientEventIdentifierConnectionFailed
                                                 userInfo:@{ NSLocalizedDescriptionKey: [NSString stringWithUTF8String:status.message.c_str()],
                                                             ClientWrapperErrorFatalKey: [NSNumber numberWithBool:YES] }];
                [self.delegate handleError:error];
            }

            OpenVPNClient::uninit_process();
        } catch(const std::exception& e) {
            NSError *error = [NSError errorWithDomain:ClientWrapperErrorDomain
                                                 code:ClientEventIdentifierConnectionFailed
                                             userInfo:@{ NSLocalizedDescriptionKey: [NSString stringWithUTF8String:e.what()],
                                                         ClientWrapperErrorFatalKey: [NSNumber numberWithBool:YES] }];
            [self.delegate handleError:error];
        }
    });
}

- (void)disconnect {
    self.vpnClient->stop();

    [self.vpnSettings removeAllObjects];
}

#pragma mark OpenVPN Wrapper

- (void)handleEvent:(const ClientAPI::Event *)event {
    NSAssert(self.delegate != nil, @"ClientWrapper delegate should be set");

    NSString *eventName = [NSString stringWithUTF8String:event->name.c_str()];
    NSString *eventInfo = [NSString stringWithUTF8String:event->info.c_str()];

    ClientEvent *recievedEvent = [ClientEvent createEvent:eventName info:eventInfo];

    if (event->error) {
        NSMutableDictionary *userInfo = [NSMutableDictionary new];
        [userInfo setObject:[NSNumber numberWithBool:event->fatal] forKey:ClientWrapperErrorFatalKey];

        if (eventInfo != nil && ![eventInfo isEqualToString:@""]) {
            [userInfo setObject:eventInfo forKey:NSLocalizedDescriptionKey];
        }

        NSError *error = [NSError errorWithDomain:ClientWrapperErrorDomain
                                             code:recievedEvent.identifier
                                         userInfo:[userInfo copy]];

        [self.delegate handleError:error];
    } else {
        [self.delegate handleEvent:recievedEvent];
    }
}

- (void)handleLog:(const ClientAPI::LogInfo *)log {

}

- (void)setRemoteAddress:(NSString *)address {
    self.vpnSettings[VPNSettingsRemoteAddressKey] = address;
}

- (void)setLocalAddress:(NSString *)address subnet:(NSString *)subnet gateway:(NSString *)gateway {
    self.vpnSettings[VPNSettingsLocalAddressKey] = address;
    self.vpnSettings[VPNSettingsSubnetMaskKey] = subnet;
    self.vpnSettings[VPNSettingsGatewayKey] = gateway;
}

- (void)addRoute:(NSString *)route subnet:(NSString *)subnet {
    NEIPv4Route *includedRoute = [[NEIPv4Route alloc] initWithDestinationAddress:route subnetMask:subnet];

    if (self.vpnSettings[VPNSettingsIncludedRoutesKey] == nil) {
        self.vpnSettings[VPNSettingsIncludedRoutesKey] = [NSMutableArray new];
    }

    NSMutableArray *includedRoutes = self.vpnSettings[VPNSettingsIncludedRoutesKey];
    [includedRoutes addObject:includedRoute];
}

- (void)excludeRoute:(NSString *)route subnet:(NSString *)subnet {
    NEIPv4Route *excludedRoute = [[NEIPv4Route alloc] initWithDestinationAddress:route subnetMask:subnet];

    if (self.vpnSettings[VPNSettingsExcludedRoutesKey] == nil) {
        self.vpnSettings[VPNSettingsExcludedRoutesKey] = [NSMutableArray new];
    }

    NSMutableArray *excludedRoutes = self.vpnSettings[VPNSettingsExcludedRoutesKey];
    [excludedRoutes addObject:excludedRoute];
}

- (void)addDNSAddress:(NSString *)address {
    if (self.vpnSettings[VPNSettingsDNSAddressesKey] == nil) {
        self.vpnSettings[VPNSettingsDNSAddressesKey] = [NSMutableArray new];
    }

    NSMutableArray *dnsAddresses = self.vpnSettings[VPNSettingsDNSAddressesKey];
    [dnsAddresses addObject:address];
}

- (void)addSearchDomain:(NSString *)domain {
    if (self.vpnSettings[VPNSettingsSearchDomainsKey] == nil) {
        self.vpnSettings[VPNSettingsSearchDomainsKey] = [NSMutableArray new];
    }

    NSMutableArray *domains = self.vpnSettings[VPNSettingsSearchDomainsKey];
    [domains addObject:domain];
}

- (void)setMTU:(NSInteger)mtu {
    self.vpnSettings[VPNSettingsMTUKey] = @(mtu);
}

- (NSInteger)establishTunnel {
    NSString *remoteAddress = self.vpnSettings[VPNSettingsRemoteAddressKey];
    NEPacketTunnelNetworkSettings *networkSettings = [[NEPacketTunnelNetworkSettings alloc] initWithTunnelRemoteAddress:remoteAddress];

    NSString *localAddress = self.vpnSettings[VPNSettingsLocalAddressKey];
    NSString *subnetMask = self.vpnSettings[VPNSettingsSubnetMaskKey];

    NEIPv4Settings *ipSettings = [[NEIPv4Settings alloc] initWithAddresses:@[localAddress] subnetMasks:@[subnetMask]];

    ipSettings.includedRoutes = self.vpnSettings[VPNSettingsIncludedRoutesKey];
    ipSettings.excludedRoutes = self.vpnSettings[VPNSettingsExcludedRoutesKey];

    networkSettings.IPv4Settings = ipSettings;

    NSArray *dnsServers = self.vpnSettings[VPNSettingsDNSAddressesKey];
    NSArray *searchDomains = self.vpnSettings[VPNSettingsSearchDomainsKey];

    if (dnsServers != nil && [dnsServers count] > 0) {
        networkSettings.DNSSettings = [[NEDNSSettings alloc] initWithServers:[dnsServers copy]];

        if (searchDomains != nil && [searchDomains count] > 0) {
            networkSettings.DNSSettings.searchDomains = searchDomains;
        }
    }

    networkSettings.MTU = self.vpnSettings[VPNSettingsMTUKey] != nil ? self.vpnSettings[VPNSettingsMTUKey] : @(1500);

    NSAssert(self.delegate != nil, @"ClientWrapper delegate should be set");

    self.packetFlow = [self.delegate setTunnelSettings:networkSettings];
    [self readTUNPackets];

    return self.packetFlow == nil ? -1 : CFSocketGetNative(self.vpnSocket);
}


#pragma mark TUN -> OpenVPN

- (void)readTUNPackets {
    [self.packetFlow readPacketsWithCompletionHandler:^(NSArray<NSData *> * _Nonnull packets, NSArray<NSNumber *> * _Nonnull protocols) {
        [packets enumerateObjectsUsingBlock:^(NSData * data, NSUInteger idx, BOOL * stop) {
            NSData *packet = [self.dataHandler handleTUNPacket:data protocol:protocols[idx]];
            CFSocketSendData(self.wrapperSocket, NULL, (CFDataRef)packet, 0.05);
        }];

        [self readTUNPackets];
    }];
}

#pragma mark OpenVPN -> TUN

- (void)readVPNData:(NSData *)data {
    NSDictionary *handled = [self.dataHandler handleVPNData:data];
    if (handled) {
        [self.packetFlow writePackets:handled[VPNDataHandlerPacketsKey]
                        withProtocols:handled[VPNDataHandlerProtocolsKey]];
    }
}

#pragma mark Deallocation

- (void)dealloc {
    delete self.vpnClient;

    CFSocketInvalidate(self.wrapperSocket);
    CFSocketInvalidate(self.vpnSocket);

    CFRelease(self.wrapperSocket);
    CFRelease(self.vpnSocket);
}

@end

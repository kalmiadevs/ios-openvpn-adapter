//
//  VPNDataHandler.m
//  Chat
//
//  Created by Sergey Abramchuk on 15.12.16.
//  Copyright © 2016 Gutch. All rights reserved.
//

#import <netinet/ip.h>

#import "VPNDataHandler.h"

NSString *const VPNDataHandlerProtocolsKey = @"VPNDataHandlerProtocolsKey";
NSString *const VPNDataHandlerPacketsKey = @"VPNDataHandlerPacketsKey";


@interface VPNDataHandler ()

@property (strong, nonatomic) NSMutableData *incompleteData;

@end

@implementation VPNDataHandler

- (instancetype)init {
    self = [super init];
    if (self) {
        self.incompleteData = nil;
    }
    return self;
}

#pragma mark Handlers

- (NSDictionary *)handleVPNData:(NSData *)data {
    NSMutableArray<NSNumber *> *protocols = [NSMutableArray new];
    NSMutableArray<NSData *> *packets = [NSMutableArray new];
    
    if (self.incompleteData) {
        [self.incompleteData appendData:data];
        data = [self.incompleteData copy];
    }
    
    NSUInteger length = [data length];
    NSUInteger offset = 0;
    
    while (offset < length) {
        if (!self.incompleteData) {
            self.incompleteData = [NSMutableData new];
        }
        
        NSUInteger beginig = offset;
        
        // Get protocol
        NSUInteger protocolSize = sizeof(uint32_t);
        if (protocolSize > length - offset) {
            NSRange leftRange = NSMakeRange(beginig, length - beginig);
            NSData *leftData = [data subdataWithRange:leftRange];
            
            [self.incompleteData appendData:leftData];
            
            break;
        }
        
        NSRange protocolRange = NSMakeRange(offset, protocolSize);
        NSData *protocolData = [data subdataWithRange:protocolRange];
        
        uint32_t protocol = [self protocolFromData:protocolData];
        offset += protocolSize;
        
        NSUInteger headerSize = sizeof(struct ip);
        if (headerSize > length - offset) {
            NSRange leftRange = NSMakeRange(beginig, length - beginig);
            NSData *leftData = [data subdataWithRange:leftRange];
            
            [self.incompleteData appendData:leftData];
            
            break;
        }
        
        // Get IP header
        NSRange headerRange = NSMakeRange(offset, headerSize);
        NSData *headerData = [data subdataWithRange:headerRange];
        
        struct ip header = [self headerFromData:headerData];
        
        // Get packet
        NSUInteger bytesLeft = length - offset;
        NSUInteger packetLength = header.ip_len;
        
        if (packetLength > bytesLeft) {
            NSRange leftRange = NSMakeRange(beginig, length - beginig);
            NSData *leftData = [data subdataWithRange:leftRange];

            [self.incompleteData appendData:leftData];
            
            break;
        }
        
        NSRange packetRange = NSMakeRange(offset, packetLength);
        NSData *packetData = [data subdataWithRange:packetRange];
        
        [protocols addObject:@(protocol)];
        [packets addObject:packetData];
        
        self.incompleteData = nil;
        
        offset += header.ip_len;
    }
    
    return [protocols count] > 0 && [packets count] > 0 ?
        @{ VPNDataHandlerPacketsKey: [packets copy], VPNDataHandlerProtocolsKey: [protocols copy] } : nil;
}

- (NSData *)handleTUNPacket:(NSData *)packet protocol:(NSNumber *)protocol {
    // Prepend data with network protocol. It should be done because OpenVPN uses uint32_t prefixes containing network protocol.
    uint32_t prefix = CFSwapInt32HostToBig((uint32_t)[protocol unsignedIntegerValue]);
    
    NSMutableData *data = [NSMutableData new];
    [data appendBytes:&prefix length:sizeof(prefix)];
    [data appendData:packet];
    
    return [data copy];
}

#pragma mark Utils

- (uint32_t)protocolFromData:(NSData *)data {
    uint32_t protocol = 0;
    
    NSAssert([data length] >= sizeof(protocol), @"Length of data should be equal or greater then size of the uint32_t");
    
    [data getBytes:&protocol length:sizeof(protocol)];
    
    return CFSwapInt32BigToHost(protocol);
}

- (struct ip)headerFromData:(NSData *)data {
    struct ip header;
    memset(&header, 0, sizeof(header));
    
    NSAssert([data length] >= sizeof(header), @"Length of data should be equal or greater then size of the ip header struct");
    
    [data getBytes:&header length:sizeof(header)];
    
    header.ip_hl = CFSwapInt32BigToHost(header.ip_hl);
    header.ip_v = CFSwapInt32BigToHost(header.ip_v);
    header.ip_len = CFSwapInt16BigToHost(header.ip_len);
    header.ip_id = CFSwapInt16BigToHost(header.ip_id);
    header.ip_off = CFSwapInt16BigToHost(header.ip_off);
    header.ip_sum = CFSwapInt16BigToHost(header.ip_sum);
    header.ip_src.s_addr = CFSwapInt32BigToHost(header.ip_src.s_addr);
    header.ip_dst.s_addr = CFSwapInt32BigToHost(header.ip_dst.s_addr);
    
    return header;
}

@end

//
//  IndexRequestHandler.swift
//  App Test Extenstion
//
//  Created by Tanja Stular on 29/03/2017.
//  Copyright © 2017 Tanja Stular. All rights reserved.
//

import NetworkExtension
import OpenVPNAdapter

enum PacketTunnelProviderError: Error {
    case fatalError(message: String)
}

class PacketTunnelProvider: NEPacketTunnelProvider {

    /*lazy var vpnAdapter: ClientWrapper = {
        return ClientWrapper().then { $0.delegate = self }
    }()*/
    var vpnAdapter: ClientWrapper!
    
    var startHandler: ((Error?) -> Void)?
    var stopHandler: (() -> Void)?
    
    
    override func startTunnel(options: [String : NSObject]? = nil, completionHandler: @escaping (Error?) -> Void) {
        
        NSLog("VPN LOG: Start OpenVPN tunnel")
        
        self.startHandler = completionHandler
        
        
        guard let settings = options?["Settings"] as? Data else {
            NSLog("VPN LOG: Error: Failed to retrieve OpenVPN settings from options")
            let error = PacketTunnelProviderError.fatalError(message: "Failed to retrieve OpenVPN settings from options")
            completionHandler(error)
            return
        }
        
        self.vpnAdapter = ClientWrapper(username: "", password: "", configurationFile: settings)
        
        if self.vpnAdapter == nil {
            let error = NSError(domain: ClientWrapperErrorDomain, code: Int(ClientEventIdentifier.initializationFailed.rawValue), userInfo: [NSLocalizedDescriptionKey: "Failed to initialize open vpn client"])
            
            NSLog("VPN LOG: Failed to initialize open vpn client")

            completionHandler(error)
            return
        }
        
        
        NSLog("VPN LOG: Connecting")

        
        self.vpnAdapter.delegate = self
        vpnAdapter.connect()
    }
    
    override func stopTunnel(with reason: NEProviderStopReason, completionHandler: @escaping () -> Void) {
        stopHandler = completionHandler
        vpnAdapter.disconnect()
    }
    
}

extension PacketTunnelProvider: ClientWrapperDelegate {
    
    func setTunnelSettings(_ settings: NEPacketTunnelNetworkSettings!) -> NEPacketTunnelFlow! {
        
        var success = true
        
        setTunnelNetworkSettings(settings) { (error) in
            
            if error != nil {
                success = false
                NSLog("KALMIA LOG: error in setting tunnel \(String(describing: error?.localizedDescription))")
            }
            
        }
        
        
        return success ? self.packetFlow : nil
        
    }

    
    public func handle(_ event: ClientEvent!) {
        
        switch event.identifier {
        case ClientEventIdentifier.connected:
            guard let startHandler = startHandler else {
                return
            }
            
            startHandler(nil)
            self.startHandler = nil
            break
        case ClientEventIdentifier.disconnected:
            guard let stopHandler = stopHandler else {
                return
            }
            
            stopHandler()
            self.startHandler = nil
            break
        default:
            break
        }
        
    }
    
    
    public func handleError(_ error: Error!) {
        guard let fatal = (error as NSError).userInfo[ClientWrapperErrorFatalKey] as? Bool, fatal == true else {
            return
        }
        
        NSLog("KALMIA: error %s", error.localizedDescription)
        
        if let startHandler = startHandler {
            startHandler(error)
            self.startHandler = nil
        } else {
            cancelTunnelWithError(error)
            self.vpnAdapter.disconnect()
        }
    }


}


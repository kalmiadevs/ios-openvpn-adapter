//
//  ViewController.swift
//  App Test
//
//  Created by Tanja Stular on 29/03/2017.
//  Copyright © 2017 Tanja Stular. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let newVpn = VPNManager()

    @IBOutlet weak var iskratelServerBtn: UIButton!
    @IBOutlet weak var kalmiaServerBtn: UIButton!
    @IBOutlet weak var disconnectBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        disconnectBtn.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.newVpn.disconnect()
    }
   
    @IBAction func kalmiaServerClicked(_ sender: Any) {
        
        if newVpn.isConnected() {
            newVpn.disconnect()
        }
        else {
            newVpn.configureVPNConfiguration(profile: "kalmia")
            kalmiaServerBtn.isHidden = true
            iskratelServerBtn.isHidden = true
            disconnectBtn.isHidden = false
        }
        
    }
    
    @IBAction func iskratelServerClicked(_ sender: Any) {
        
        if newVpn.isConnected() {
            newVpn.disconnect()

        }
        else {
            newVpn.configureVPNConfiguration(profile: "kriz")
            kalmiaServerBtn.isHidden = true
            iskratelServerBtn.isHidden = true
            disconnectBtn.isHidden = false

        }
        
    }
    @IBAction func disconnect(_ sender: Any) {
        if newVpn.isConnected() {
            newVpn.disconnect()
            disconnectBtn.isHidden = true
            kalmiaServerBtn.isHidden = false
            iskratelServerBtn.isHidden = false
            
        }
    }
}


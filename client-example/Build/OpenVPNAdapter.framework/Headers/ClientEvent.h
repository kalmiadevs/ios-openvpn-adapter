//
//  ClientEvent.h
//  OpenVPN NEF Test
//
//  Created by Sergey Abramchuk on 05.11.16.
//  Copyright © 2016 ss-abramchuk. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef NS_ENUM(NSUInteger, ClientEventIdentifier) {
    ClientEventIdentifierDisconnected,
    ClientEventIdentifierConnected,
    ClientEventIdentifierReconnecting,
    ClientEventIdentifierResolve,
    ClientEventIdentifierWait,
    ClientEventIdentifierWaitProxy,
    ClientEventIdentifierConnecting,
    ClientEventIdentifierGetConfig,
    ClientEventIdentifierAssignIP,
    ClientEventIdentifierAddRoutes,
    ClientEventIdentifierEcho,
    ClientEventIdentifierInfo,
    ClientEventIdentifierPause,
    ClientEventIdentifierResume,
    ClientEventIdentifierTransportError,
    ClientEventIdentifierTunError,
    ClientEventIdentifierClientRestart,
    ClientEventIdentifierAuthFailed,
    ClientEventIdentifierCertVerifyFail,
    ClientEventIdentifierTLSVersionMin,
    ClientEventIdentifierClientHalt,
    ClientEventIdentifierConnectionTimeout,
    ClientEventIdentifierInactiveTimeout,
    ClientEventIdentifierDynamicChallenge,
    ClientEventIdentifierProxyNeedCreds,
    ClientEventIdentifierProxyError,
    ClientEventIdentifierTunSetupFailed,
    ClientEventIdentifierTunIfaceCreate,
    ClientEventIdentifierTunIfaceDisabled,
    ClientEventIdentifierEPKIError,
    ClientEventIdentifierEPKIInvalidAlias,
    ClientEventIdentifierInitializationFailed,
    ClientEventIdentifierConnectionFailed,
    ClientEventIdentifierUnknown
};


@interface ClientEvent : NSObject

@property (readonly, nonatomic) ClientEventIdentifier identifier;
@property (readonly, strong, nonatomic) NSString *info;

+ (ClientEvent *)createEvent:(NSString *)name info:(NSString *)info;

@end

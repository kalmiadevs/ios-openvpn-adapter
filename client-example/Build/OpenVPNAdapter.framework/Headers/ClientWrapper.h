//
//  ClientWrapper.h
//  Chat
//
//  Created by Sergey Abramchuk on 07.12.16.
//  Copyright © 2016 Gutch. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ClientEvent.h"

@class NEPacketTunnelFlow;
@class NEPacketTunnelNetworkSettings;

extern NSString *const ClientWrapperErrorDomain;

extern NSString *const ClientWrapperErrorFatalKey;

@protocol ClientWrapperDelegate <NSObject>

- (NEPacketTunnelFlow *)setTunnelSettings:(NEPacketTunnelNetworkSettings *)settings;
- (void)handleEvent:(ClientEvent *)event;
- (void)handleError:(NSError *)error;

@end


@interface ClientWrapper : NSObject

@property (weak, nonatomic) id<ClientWrapperDelegate> delegate;

- (instancetype)initWithUsername:(NSString *)username
                        password:(NSString *)password
               configurationFile:(NSData *)configurationFile;

- (void)connect;
- (void)disconnect;

@end

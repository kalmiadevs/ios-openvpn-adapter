//
//  VPNManager.swift
//  App Test
//
//  Created by Tanja Stular on 01/04/2017.
//  Copyright © 2017 Tanja Stular. All rights reserved.
//

import Foundation
import NetworkExtension

class VPNManager {
    
    var manager: NETunnelProviderManager!
    var options: [String: NSObject]!
    
    func isConnected() -> Bool {
        if self.manager == nil {
            return false
        }
        return (self.manager.connection.status == NEVPNStatus.connected)
    }
    
    init() {
        self.observeStatus()
    }
    
    deinit {
        self.stopObservingStatus()
    }
    
    /// Register for configuration change notifications.
    func observeStatus() {
        
            NotificationCenter.default.addObserver(forName: NSNotification.Name.NEVPNStatusDidChange, object: nil, queue: nil, using: { notification in
                
                if self.manager != nil {
                    switch self.manager.connection.status {
                    case NEVPNStatus.connecting:
                        NSLog("=== VPN Connecting ===")
                        break
                    case NEVPNStatus.connected:
                        NSLog("=== VPN Connected ===")
                        break
                    case NEVPNStatus.disconnecting:
                        NSLog("=== VPN Disconnecting ===")
                        break
                    case NEVPNStatus.disconnected:
                        NSLog("=== VPN Disconnected ===")
                        break
                    default:
                        break
                    }
                }
                
            })
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillTerminate, object: nil, queue: nil, using: { notification in
            // disconect
            self.disconnect()
        })
        
    }
    
    /// De-register for configuration change notifications.
    func stopObservingStatus() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.NEVPNStatusDidChange, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIApplicationWillTerminate, object: nil)
    }
    
    func configureVPNConfiguration(profile: String) -> Void {
        
        NETunnelProviderManager.loadAllFromPreferences { (_ managers: [NETunnelProviderManager]?, _ error: Error?) in
            
            if error != nil {
                NSLog("Error: \(String(describing: error))")
            }
            else {
                
                if managers != nil && managers?.first != nil {
                    self.manager = managers?.first
                }
                else {
                    self.manager = NETunnelProviderManager()
                }
                
                
                self.options = ["Settings": self.getVPNOptions(profile: profile) as NSData]
                
                self.manager.protocolConfiguration = self.configureVPNProtocol()
                
                if let appName = Bundle.main.infoDictionary![kCFBundleNameKey as String] as? String {
                    self.manager.localizedDescription = appName
                }
                else {
                    self.manager.localizedDescription = "Iskratel X10"
                }
                
                self.manager.isEnabled = true
                
                self.manager.saveToPreferences(completionHandler: { (error) in
                    if error != nil {
                        NSLog("Error: \(String(describing: error))")
                    }
                    else {
                        NSLog("VPN preferences successfully saved")
                        self.connect()
                    }
                })
                
            }
        }
        
    }
    
    func configureVPNProtocol() -> NETunnelProviderProtocol{
        
        let protocolConf = NETunnelProviderProtocol()
        protocolConf.providerBundleIdentifier = "tnt.App-Test.App-Test-Extenstion"
        protocolConf.serverAddress = "194.249.118.51:443"
        protocolConf.disconnectOnSleep = false
        return protocolConf
    }
    
    
    func getVPNOptions(profile: String) -> Data {
        guard
            let path = Bundle.main.url(forResource: profile, withExtension: "ovpn"),
            let configuration = try? Data(contentsOf: path)
            else {
                fatalError("Failed to retrieve OpenVPN configuration")
        }
        
        return configuration
    }
    
    
    func connect() -> Void {
        
        self.manager.loadFromPreferences { (error) in
            
            if error != nil {
                NSLog("Error loading: \(String(describing: error))")
            }
            
            else {
                do {
                    try self.manager.connection.startVPNTunnel(options: self.options!)
                }
                catch {
                    NSLog("Error connecting vpn: \(error)")
                }
            }
        }
    }
    
    func disconnect() -> Void {
        self.manager.connection.stopVPNTunnel()
    }
    
}



